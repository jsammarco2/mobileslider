const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const fs = require('fs');
const xml2js = require('xml2js');
const request = require('request');
var config = {},
	configData = "",
	configJsonFile = "config.json";

configData = fs.readFileSync(configJsonFile);
config = JSON.parse(configData);

if (!config || !config.Sliders || !config.General) {
	console.log("Error parsing "+configJsonFile+". No Sliders object or General object.");
	process.exit();
}
console.log("CONFIG:", config);

app.use('/static', express.static(path.join(__dirname, 'public')));

app.get("/", function(req, res){
	res.send("Must use Slider Path. No Slider Here. :/");
});

for (var i = config.Sliders.length - 1; i >= 0; i--) {//Setup routes for each stat
	addStatRoute(config.Sliders[i]);
}

function pushWCtoCX(stat, wc, cb){
	console.log("Request to update WC:", 'http://'+stat.statDataHost+':'+(stat.statDataPort || 80)+stat.statDataUpdatePath);
	console.log({
		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
		url:     'http://'+stat.statDataHost+stat.statDataUpdatePath,
		body:    "zAction=Submit&zName=1&zValue="+wc
	});
	request.post({
		headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
		url:     'http://'+stat.statDataHost+stat.statDataUpdatePath,
		body:    "zAction=Submit&zName=1&zValue="+wc
	}, function(error, response, body){
		if (error) { return console.log("Error from CX:", error.toString()); }
		console.log("Response from CX:", body);
    	cb(stat, body, response.statusCode);
	});
}

function wcUpdate(req, res){
	var wc_update = parseFloat(req.params.wc),
		foundIndex = 0,
		found = config.Sliders.find(function(slider) {
			return req.params.id == slider.id;
			foundIndex += 1;
		});
	console.log("wcUpdate", req.params.id, wc_update);
	if(!found){
		console.log("Stat not found");
		return res.sendStatus(404);
	}
	if (wc_update >= (found.min || -3) && wc_update <= (found.max || 3)) {
		if (found.sbo) {
			if(!found.setpoint_warmercooler || found.setpoint_warmercooler == "warmercooler"){
				found.wc = wc_update;
			}else{
				found.tempSP = wc_update;
			}
			found.remote_update = true;
			clearTimeout(found.remote_update_timer);
			found.remote_update_timer = setTimeout(function () { found.remote_update = false; }, 10000);
			res.sendStatus(200);
			return console.log("Update SBO Data");
		}
		if(!found.setpoint_warmercooler || found.setpoint_warmercooler == "warmercooler"){
			found.tempWC = wc_update;
		}else{
			found.tempSP = wc_update;
		}
		pushWCtoCX(found, wc_update, function(stat, result, statusCode){
			console.log("Pushed WC", statusCode, result);
		});
		console.log("/wc/:id/:wc Update Value:", foundIndex, wc_update);
		return res.sendStatus(200);
	}
	console.log("Out of Range", wc_update, found.min, found.max);
	res.sendStatus(400);
}

function pushUpdate(req, res){
	var stat_id = req.params.id,
		data = decodeURIComponent(req.params.data),
		foundIndex = 0,
		found = config.Sliders.find(function(slider) {
			return stat_id == slider.id;
			foundIndex += 1;
		});
	console.log(stat_id, data);
	var json = JSON.parse(data);
	if (!json) { res.sendStatus(400); return console.log("Error, JSON invalid", json, data); }
	found.temp = json.temp || 0;
	if (found.remote_update) {
		console.log("Skip update SP");
	}else{
		found.tempSP = json.tempSP || 0;
		found.tempWC = json.tempWC || 0;
	}
	res.sendStatus(200);
}

function addStatRoute(slider) {
	app.get(slider.sliderPath, function(req, res){
		fs.readFile(__dirname + '/public/index.html', "utf8", function(err, data){
		    if(err) throw err;
			fs.readFile(__dirname + '/layouts/'+(slider.layout || "default")+'.html', "utf8", function(err, layoutData){
				if(err) {
					console.error("No Layout File Found for:", (slider.layout || "default"));
					throw err;
				}
				if (slider.wc > slider.max || slider.wc < slider.min) { slider.wc = Math.ceil(slider.max + slider.min / 2); }
			    //Replace tokens with values
			    data = data.replace("{{clientUpdateInterval}}", (slider.clientUpdateInterval || 5000));
			    data = data.replace("{{setpoint_warmercooler}}", (slider.setpoint_warmercooler || "warmercooler"));
			    data = data.replace("{{layout}}", layoutData);
			    data = data.replace("{{layoutName}}", (slider.layout || "default"));
			    data = data.replace("{{appName}}", slider.app_name);
			    data = data.replace(new RegExp("{{name}}", 'g'), slider.name);
			    data = data.replace("{{dataId}}", slider.id);
			    data = data.replace("{{min}}", slider.min || -3);
			    data = data.replace("{{max}}", slider.max || 3);
			    data = data.replace("{{step}}", slider.step || 1);
			    data = data.replace("{{wc}}", slider.wc);
			    res.send(data);
			});
		});
	});
	app.get('/wc/:id/:wc', wcUpdate);
	if (slider.sbo) { app.get('/pushData/:id/:data', pushUpdate); }
}

function refreshRemoteData() {
	var lastPolled = null;
	for (var i = config.Sliders.length - 1; i >= 0; i--) {
		if(!config.Sliders[i].statDataHost || !config.Sliders[i].statDataPath){
			console.log("Remote URL is undefined", config.Sliders[i]); continue;
		}
		if (config.Sliders[i].sbo) { console.log("Don't check. SBO enabled for", config.Sliders[i].name); continue; }
		lastPolled = (new Date() - config.Sliders[i].lastPolled) / 1000;
		//console.log("Last Polled:", config.Sliders[i].name, lastPolled);
		if(lastPolled > 15 || !config.Sliders[i].lastPolled){ console.log("Skip polling", config.Sliders[i].name); continue; }
		remoteDataRequest(config.Sliders[i], function cb(stat, data){
			var parser = new xml2js.Parser();
			parser.parseString(data, function (err, parsedXml) {
				if (err || !parsedXml || parsedXml === null || !parsedXml.items) {
					return console.log("Failed to parse XML from "+stat.statDataHost+":"+(stat.statDataPort || 80)+stat.statDataPath);
				}
				if (parseFloat(parsedXml.items.temp) >= -400 && parseFloat(parsedXml.items.temp) <= 400) {
					stat.temp = parseFloat(parsedXml.items.temp);
					stat.tempSP = parseFloat(parsedXml.items.tempSP);
					stat.tempWC = parseFloat(parsedXml.items.tempWC);
					console.log("Getting Data From CX "+stat.name+":", stat.temp, stat.tempSP, stat.tempWC);
				}else{
					console.log("NOT IN RANGE");
				}
		    });
		});
	}
}

function remoteDataRequest(stat, cb){
	//console.log("Requesting: "+stat.statDataHost+":"+(stat.statDataPort || 80)+stat.statDataPath);
	request.get({
		url:     'http://'+stat.statDataHost+':'+(stat.statDataPort || 80)+stat.statDataPath
	}, function(error, response, body){
		if (error) { return console.log("Error from CX:", error.toString()); }
		//console.log("Response from CX:", body);
    	cb(stat, body);
	});
}

app.get('/clientData/:id', function(req, res){
	var found = config.Sliders.find(function(slider) {
		return req.params.id == slider.id;
	});
	//console.log("FOUND", found);
	if(!found){
		return res.sendStatus(404);
	}
	found.lastPolled = new Date();
	var ip = req.headers['x-real-ip'] || req.connection.remoteAddress;
	console.log("Polling", found.name, found.lastPolled, ip);
	var temp = found.temp || 0,
		tempSP = found.tempSP || 0,
		tempWC = found.tempWC || 0,
		remote_update = found.remote_update || false;
	console.log("remote_update", remote_update);
	res.send('<!--?xml version="1.0" encoding="utf-8"?--><items><temp>'+temp+'</temp><tempSP>'+tempSP+'</tempSP><tempWC>'+tempWC+'</tempWC><remoteUpdate>'+remote_update+'</remoteUpdate></items>');
});

setInterval(refreshRemoteData, config.General.dataPollInterval || 5000);

// app.get('/wc/:wc', function(req, res){
// 	var wc_update = parseInt(req.params.wc);
// 	if (wc_update >= -3 && wc_update <= 3) {
// 		wc = wc_update;
// 		tempWC = wc;//DATA TO CLIENT
// 		console.log("/wc/:wc Update Value:", wc);
// 		return res.sendStatus(200);
// 	}
// 	res.sendStatus(404);
// 	//res.send("<form><input type='input' name='wc' value='"+wc+"' /><input type='submit' /></form>");
// });



// app.get('/xml', function(req, res){
// 	res.send('<!--?xml version="1.0" encoding="utf-8"?--><items><temp>'+temp+'</temp><sp>'+tempSP+'</sp><wc>'+tempWC+'</wc></items>');
// });



// app.get('/', function(req, res){
// 	fs.readFile(__dirname + '/public/index.html', "utf8", function(err, data){
// 	    if(err) throw err;
// 	    data = data.replace("{{wc}}", wc);//Replace token with value
// 	    res.send(data);
// 	});
// });

app.listen(config.General.port || 3000, () => console.log('Example app listening on port '+(config.General.port || 3000)+'!'));