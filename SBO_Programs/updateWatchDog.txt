'Needs a numeric to trigger the sbo_stat_data_push program to run.
'This program sets the interval of how often the program runs.

Numeric Output pushUpdates

Goto push

push:
	pushUpdates = On
	Goto waitABit
	
waitABit:
	if TS > 2 Then pushUpdates = Off
	If TS > 9 Then Goto push
