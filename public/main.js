var config = {
		"xmlEndpoint": "/clientData",
		"intervalTime": 5000
	},
	temp = -99,
	tempSP = -99,
	iOSDevice = !!navigator.platform.match(/iPhone|iPod|iPad/),
	iOSApp = window.navigator.standalone == true,
	readyToUpdate = true,
	readyToUpdateTimer = null;

function updatedSlider(){
	readyToUpdate = false;
	console.log(config.intervalTime);
	clearTimeout(readyToUpdateTimer);
	readyToUpdateTimer = setTimeout(function () {
		readyToUpdate = true;
		console.log("readyToUpdate", readyToUpdate);
	}, config.intervalTime);
}

$(function(){

	config.intervalTime = parseInt($('#clientUpdateInterval').val()) || 5000;
	window.scrollTo(0,1);//Hide Android Top Bar
	if (iOSDevice && !iOSApp) {
		$('#hideAddtoHome').parent().show();
	}
	$('#hideAddtoHome').click(function (){
		$(this).parent().hide();
	});

	config.setpoint_warmercooler = $('#setpoint_warmercooler').val() == "setpoint" ? "setpoint" : "warmercooler";
	config.xmlEndpoint += "/" + $('#dataId').val();
	console.log("Config", config);

	if (config.setpoint_warmercooler == "setpoint") {
		$('.wc_item').hide();
	}

	function updateTemp() {
		if (!readyToUpdate) { return console.log("Delay"); }
		$('#loading_msg').show();
		$('#offline_msg').hide();
		$.get(config.xmlEndpoint, function (data, err) {
			if (err.toString() != "success") {
				$('#offline_msg').show();
				$('#loading_msg').hide();
				return console.log("Update Temp", err.toString());
			}
			$('#offline_msg').hide();
			var xmlDoc = $.parseXML(data);
			var $xml = $(xmlDoc);
			temp = $xml.find("temp");
			tempSP = $xml.find("tempSP");
			tempWC = $xml.find("tempWC");
			$("#roomTemp").html(temp.html()+"&deg;F");
			if (readyToUpdate) {
				var lastVal = $('#vert').val();
				if (config.setpoint_warmercooler == "setpoint") {
					$("#roomSP").html(tempSP.html()+"&deg;F");
					$('#vert').val(tempSP.html());
					if(lastVal != tempSP.html()){
						$('#vert').trigger('change');
					}
				}else{
					$("#roomWC").html(tempWC.html()+"&deg;F");
					$('#vert').val(tempWC.html());
					if(lastVal != tempWC.html()){
						$('#vert').trigger('change');
					}
				}
			}
			if(config.setpoint_warmercooler !== "setpoint"){
				$("#roomSP").html(tempSP.html()+"&deg;F");
			}
			setTimeout(function () {
				$('#loading_msg').hide();
			}, 500);
		}).fail(function() {
			$('#offline_msg').show();
			$('#loading_msg').hide();
			return console.log("Update Temp", err.toString());
		});
	}

	$('#vert').change(function () {
		//console.log("Change");
		if (config.setpoint_warmercooler == "setpoint") {
			$("#roomSP").html($('#vert').val()+"&deg;F");
		}else{
			$("#roomWC").html($('#vert').val()+"&deg;F");
		}
	});

	updateTemp();
	tempSubInterval = setInterval(updateTemp, config.intervalTime || 5000);
});